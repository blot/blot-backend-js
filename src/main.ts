import express, { Application } from 'express'
import { isInDebugMode } from './utils/debug'
import graphqlHTTP from 'express-graphql'
import declareGQLRoute from './routes/GQLRoute';
import { connectToDatabase, connectToContentDatabase } from './data/db/db';
import { declareContentRoute } from './routes/ContentRoute';
import { initializeDBs, initializeContentDBs } from './data/db/DBs';
import { declareAuthRoute } from './routes/AuthRoute';
import bodyParser from 'body-parser'
import cors from 'cors'

if (isInDebugMode()) {
    console.log("Server Starting in DEBUG Mode")
} else {
    console.log("Server Starting in PRODUCTION Mode... Beware")
}

Promise.all([
    connectToDatabase().then((db) => initializeDBs(db)),
    connectToContentDatabase().then((contentDB) => initializeContentDBs(contentDB))
]).then(() => {

    var app = express()

    applyMiddlewares(app)

    initializeRoutes(app)

    const listener = app.listen(process.env.PORT || 2299, () => {
        console.log(`Server has started listening in port ${listener.address().port}`)
    })

})

function initializeRoutes(app: Application) {
    console.log("Initializing Routes")

    declareGQLRoute(app)
    console.log("Initialized GQL Route")

    declareContentRoute(app)
    console.log("Initialized Content Route")

    declareAuthRoute(app)
    console.log("Initialized Auth Route")

    console.log("Initialized Routes")
}

function applyMiddlewares(app: Application) {
    console.log("Applying middlewares")

    app.use(bodyParser.json({
        limit: '10mb'
    }))
    console.log("Applied Body Parser JSON middleware")

    app.use(cors());
    console.log("Applied CORS middleware")

    console.log("Applied all middlewares")
}
