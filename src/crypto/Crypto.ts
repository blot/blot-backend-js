import crypto from 'crypto'
import bcrypt from 'bcrypt'

export default class BlotCrypto {

    // NOTE : salt string will be twice the byte length 
    static getSalt(byteLength: number) {
        return crypto.randomBytes(byteLength).toString('hex')
    }

    static getCryptoSalt(): Promise<string> {
        return bcrypt.genSalt(10)
    }

    static cryptoHash(data: string, salt: string): Promise<string> {
        return bcrypt.hash(data, salt)
    }

}