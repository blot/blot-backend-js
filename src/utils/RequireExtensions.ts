import fs from 'fs'

export function setRequireExtensions() {

    // GraphQL schemas
    require.extensions[".graphql"] = (module, filename) => {
        module.exports = fs.readFileSync(filename, 'utf-8')
    }

}