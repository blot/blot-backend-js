import { Duplex, Stream } from 'stream'

export function bufferToStream(buffer: Buffer): Stream {
    let stream = new Duplex()
    stream.push(buffer)
    stream.push(null)
    return stream
}