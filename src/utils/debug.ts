const DEBUG_MODE: boolean = process.env.PORT ? false : true

export function isInDebugMode() {
    return DEBUG_MODE
}