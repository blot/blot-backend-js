export const enum PostContentType {

    rawText = "rawText"

}

export function resolvePostContentType(contentType: string): PostContentType {

    switch (contentType) {
        case "rawText":
            return PostContentType.rawText 
        
        default: 
            return PostContentType.rawText
    }

}