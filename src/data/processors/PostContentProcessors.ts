import { PostContentType } from './PostContentType'
import PostContentProcessor from './PostContentProcessor';
import RawTextContentProcessor from './RawTextContentProcessor';
import Post from '../models/Post';

const processors = new Map<PostContentType.rawText, PostContentProcessor>([
    
    [PostContentType.rawText, new RawTextContentProcessor()]

])


export function processContent(post: Post): Promise<Post> {
    return new Promise(
        (resolve, reject) => {
            
            if (processors.has(post.contentType)) {

                resolve(processors.get(post.contentType).processContent(post))

            } else {
                console.warn(`PostContentProcessors : Undefined content type '${post.contentType}' found`)
                
                reject("No such content type")
            }

        }
    )
}

export function postProcessContent(post: Post): Promise<void> {
    return new Promise(
        (resolve, reject) => {

            if (processors.has(post.contentType)) {

                resolve(processors.get(post.contentType).postProcessContent(post))

            } else {
                console.warn(`PostContentProcessors [postProcess] : Undefined content type '${post.contentType}' found`)
                
                reject("No such content type")
            }

        }
    )
}