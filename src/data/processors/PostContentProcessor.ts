import { PostContentType } from "./PostContentType";
import Post from "../models/Post";

export default abstract class PostContentProcessor {

    /**
     * Called just before the post is posted.
     * 
     * Use for generating metadata, uploading embedded photos to target etc.
     * @param post 
     */
    abstract processContent(post: Post): Promise<Post>;

    /**
     * Called after post is live and the response is returned
     * 
     * Use for posting to hashtags and mentions etc.
     * @param post The live post
     */
    abstract postProcessContent(post: Post): Promise<void>;
}