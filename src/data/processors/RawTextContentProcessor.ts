import PostContentProcessor from "./PostContentProcessor";
import { PostContentType } from "./PostContentType";
import Post from "../models/Post";

export default class RawTextContentProcessor extends PostContentProcessor {

    processContent(post: Post): Promise<Post> {
        return Promise.resolve(post);
    }

    postProcessContent(post: Post): Promise<void> {
        throw new Error("Method not implemented.");
    }
}