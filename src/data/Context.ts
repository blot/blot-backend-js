import { SessionDB, UserDB } from "./db/DBs";
import { Request } from "express";

export interface Context {

    request: Request

}

export function getContextUser(context: Context): Promise<User> {
    return new Promise(
        (resolve, reject) => {
            getContextUserSession(context)
                .then((session) => {
                    UserDB.getUser(session.ownerID)
                        .then((user) => resolve(user))
                        .catch((err) => reject(err))
                })
                .catch((err) => reject(err))
        }
    )
}

export function getContextUserSession(context: Context): Promise<Session> {
    return new Promise(
        (resolve, reject) => {
            const sessionToken = context.request.header("Authorization")

            if (sessionToken) {
                
                SessionDB.getSession(sessionToken)
                    .then((session) => resolve(session))
                    .catch((err) => reject(err))

            } else reject("No User Session")
        }
    )
}

export function makeContext(req: Request): Context {
    return {
        request: req
    }
}