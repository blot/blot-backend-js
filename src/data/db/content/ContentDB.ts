import { Db, GridFSBucket } from "mongodb";

export default abstract class ContentDB {

    protected bucketName: string
    protected db: Db
    protected bucket: GridFSBucket

    constructor(bucketName: string, db: Db) {
        this.bucketName = bucketName
        this.db = db
        this.bucket = new GridFSBucket(db, {
            bucketName: this.bucketName
        })
    }

}