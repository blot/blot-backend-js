import ContentDB from "./ContentDB"
import { Db, GridFSBucketReadStream } from "mongodb"
import sharp from "sharp"
import fs from 'fs'
import { bufferToStream } from "../../../utils/BufferUtils";

export default class ProfilePicContentDB extends ContentDB {

    constructor(db: Db) {
        super("prof_pic", db)
    }

    getProfilePictureStream(username: string, size: "original" | "preview" | "mini", onError: (err) => void): GridFSBucketReadStream {
        
        return this.bucket.openDownloadStreamByName(`${size}_${username}`).on(
            "error", (err) => onError(err)
        )

    }

    setProfilePicture(username: string, profilePicB64: string): Promise<void> {
        return new Promise(
            async (resolve, reject) => {
                
                try {
                    const profilePicWithStrippedHeaders = profilePicB64.replace(/^data:image\/[a-z]+;base64,/, "")

                    const original = Buffer.from(profilePicWithStrippedHeaders, 'base64')
                
                    const originalStream = bufferToStream(original)
                    const miniStream = bufferToStream(await sharp(original).resize(64, 64).toBuffer())
                    const previewStream = bufferToStream(await sharp(original).resize(128, 128).toBuffer())

                    originalStream.pipe(this.bucket.openUploadStream(`original_${username}`))
                    miniStream.pipe(this.bucket.openUploadStream(`mini_${username}`))
                    previewStream.pipe(this.bucket.openUploadStream(`preview_${username}`))

                    resolve()
                } catch (e) {
                    reject(e)
                }
                
            }
        )
    }

}