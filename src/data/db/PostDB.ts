import DataDB from "./DataDB";
import { Db } from "mongodb";
import BlotCrypto from "../../crypto/Crypto";
import { PostContentType } from "../processors/PostContentType";
import { processContent, postProcessContent } from "../processors/PostContentProcessors";
import Post from "../models/Post";

export default class PostDB extends DataDB {

    constructor(db: Db) {
        super("posts", db)
    }

    getPost(id: string): Promise<Post> {

        return new Promise(
            (resolve, reject) => {
                this.collection.findOne({ "_id": id},
                    (err, data) => {

                        if (err) reject(err)
                        else resolve({
                            id: data._id,
                            postedOn: data.postedOn,
                            postedBy: data.postedBy,
                            contentB64: data.contentB64,
                            contentType: data.contentType || PostContentType.rawText
                        })

                    }
                )
            }
        )

    }

    createPost(contentType: PostContentType, contentB64: string, username: string): Promise<Post> {

        return new Promise(
            async (resolve, reject) => {
                const id = await this.generateUniquePostID()

                const post: Post = {
                    id: id,
                    postedOn: new Date(Date.now()).toISOString(),
                    postedBy: username,
                    contentB64: contentB64,
                    contentType: contentType
                }
                
                try {
                    const processedPost = await processContent(post)

                    if (processedPost) {
                        this.collection.insertOne({
                            _id: processedPost.id,
                            postedOn: processedPost.postedOn,
                            postedBy: processedPost.postedBy,
                            contentB64: processedPost.contentB64,
                            contentType: processedPost.contentType
                        }, (err, res) => {
                            if (err) reject(err)

                            postProcessContent(post).catch((err) => console.log(err))

                            resolve(processedPost)
                        })
                    }
                } catch (e) {
                    reject(e)
                }
            }
        )

    }

    private async generateUniquePostID(): Promise<string> {
        var id = BlotCrypto.getSalt(20)
        var tokenMatchCount = await this.collection.count({ "_id": id })
        while (tokenMatchCount > 0) {
            console.warn("PostDB : generateUniquePostID, generated id collision")
            id = BlotCrypto.getSalt(20)
            tokenMatchCount = await this.collection.count({ "_id": id })
        }
        
        return id
    }

    getPostsByUser(id: string, offset: number, limit: number): Promise<Post[]> {
        
        return new Promise(
            (resolve, reject) => {
                
                var list: Post[] = []

                this.collection.find({ "postedBy": id })
                    .sort({ "postedOn": -1 })
                    .skip(offset)
                    .limit(limit)
                    .forEach(
                        (doc) => {
                            list.push({
                                id: doc._id,
                                postedOn: doc.postedOn,
                                postedBy: doc.postedBy,
                                contentB64: doc.contentB64,
                                contentType: doc.contentType || PostContentType.rawText
                            })
                        },
                        () => {
                            resolve(list)
                        }
                    )
            }
        )

    }



}