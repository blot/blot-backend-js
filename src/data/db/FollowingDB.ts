import DataDB from "./DataDB";
import { Db } from "mongodb";

export default class FollowingDB extends DataDB {

    constructor(db: Db) {
        super("following", db)
    }

    getFollowingCount(username: string): Promise<number> {

        return new Promise(
            (resolve, reject) => {
                this.collection.aggregate([
                    { $match: { "_id": username } },
                    { $limit: 1 },
                    { $project: { count: { $size: "$following" } } }
                ], (err, result) => {
                    if (err) reject(err)
                    else {
                        result.each((err, data) => {
                            if (err) reject(err)
                            else resolve(data ? data.count : 0)
                        })
                    }
                })
            }
        )

    }

    getIfFollowing(username: string, toCheckUsername: string): Promise<boolean> {
        return new Promise(
            (resolve, reject) => {

                if (toCheckUsername === username) resolve(true)
                else {
                    this.collection.count({ "_id": username, "following": toCheckUsername }, (err, count) => {
                        if (err) reject(err)
    
                        if (count > 0) resolve(true)
                        else resolve(false)
                    })
                }
            }
        )
    }

    addFollowing(username: string, toAddUsername: string): Promise<number> {
        return new Promise(
            (resolve, reject) => {
                this.collection.count({ "_id": username }, (err, count) => {

                    // Already has following entry
                    if (count > 0) {
                        this.collection.updateOne({ "_id": username }, { "$push": { "following": toAddUsername } }).then(() => {
                            resolve(this.getFollowingCount(username))
                        })
                    } else { // Doesn't have a following entry
                        this.collection.insertOne(
                            {
                                "_id": username,
                                "following": [
                                    toAddUsername
                                ]
                            }, (err) => {
                                if (err) reject(err)

                                resolve(this.getFollowingCount(username))
                            })
                    }

                })
            }
        )
    }

    removeFollowing(username: string, toRemoveUsername: string): Promise<number> {
        return new Promise(
            (resolve, reject) => {

                this.collection.count({ "_id": username }, (err, count) => {
                    if (err) reject(err)


                    if (count > 0) {
                        this.collection.updateOne({ "_id": username }, { "$pull": { "following": toRemoveUsername } }, (err, res) => {
                            if (res.modifiedCount > 0) resolve(this.getFollowingCount(username))
                            else reject("No following removed")
                        })
                    } else reject("User entry doesn't exist (followingCount might be 0)")

                })

            }
        )
    }

}