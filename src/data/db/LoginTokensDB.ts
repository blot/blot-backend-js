import DataDB from "./DataDB";
import { Db } from "mongodb";
import BlotCrypto from "../../crypto/Crypto";

export default class LoginTokensDB extends DataDB {

    constructor(db: Db) {
        super("logintokens", db)
    }

    getToken(id: string, refreshToken: Boolean = true): Promise<LoginToken> {
        
        return new Promise(
            (resolve, reject) => {
                this.collection.findOne({ "_id": id })
                    .then((data) => {
                        if (data) {
                            this.collection.updateOne({ "_id": id }, { "lastRefreshedAt": new Date(Date.now()).toISOString() })

                            resolve({
                                token: data._id,
                                issuedAt: data.issuedAt,
                                issuedTo: data.issuedTo,
                                lastRefreshedAt: refreshToken ? new Date(Date.now()).toISOString() : data.lastRefreshedAt
                            })
                        } reject("Couldn't find Login Token with ID")
                    })
            }
        )

    }

    createToken(username: string): Promise<LoginToken> {
        return new Promise(
            async (resolve, reject) => {

                const tokenID = await this.generateUniqueToken()
                const issueDateString = new Date(Date.now()).toISOString()

                this.collection.insertOne({
                    "_id": tokenID,
                    issuedAt: issueDateString,
                    issuedTo: username,
                    lastRefreshedAt: issueDateString
                }).then(() => {
                    resolve({
                        token: tokenID,
                        issuedAt: issueDateString,
                        issuedTo: username,
                        lastRefreshedAt: issueDateString,
                    })
                }).catch((err) => reject(err))

            }
        )
    }

    async generateUniqueToken(): Promise<string> {
        var token = BlotCrypto.getSalt(15)
        var tokenMatchCount = await this.collection.count({ "_id": token })
        while (tokenMatchCount > 0) {
            console.warn("LoginTokensDB : generateUniqueToken, generated token collision")
            token = BlotCrypto.getSalt(15)
            tokenMatchCount = await this.collection.count({ "_id": token })
        }
        return token
    }

}