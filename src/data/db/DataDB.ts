import { Collection, Db } from "mongodb";

export default abstract class DataDB {

    protected collectionName: string
    protected collection: Collection
    protected db: Db

    constructor(collectionName: string, db: Db) {
        this.collectionName = collectionName
        this.collection = db.collection(collectionName)
        this.db = db
    }

}