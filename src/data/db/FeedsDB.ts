import { Db } from "mongodb";
import DataDB from "./DataDB";
import { PostDB, FollowersDB } from "./DBs";
import Post from "../models/Post";

export default class FeedsDB extends DataDB {

    constructor(db: Db) {
        super("feeds", db)
    }

    getFeedPosts(username: string, offset: number, limit: number): Promise<Post[]> {

        return new Promise(
            (resolve, reject) => {

                var result: Post[] = []

                this.collection.find({ "_id": username }).project({ "_id": 0, "feed": { $slice: [offset, limit] } }).limit(1).forEach(
                    (doc) => {
                        if (doc) {
                            if ((doc.feed as string[]).length > 0) {
                                var counter = 0;
                                (doc.feed as string[]).forEach((postID) => {
                                    PostDB.getPost(postID)
                                        .then((post) => {
                                            result.push(post)
                                            counter++

                                            if (counter == (doc.feed as string[]).length) {
                                                resolve(result)
                                            }
                                        })
                                        .catch(
                                            (err) => {
                                                console.error("FeedsDB: Error when processing post")
                                                counter++

                                                if (counter == (doc.feed as string[]).length) {
                                                    resolve(result)
                                                }
                                            }
                                        )
                                })
                            } else resolve([])
                        } else resolve([])

                    }, () => {} 
                )
            }
        )

    }

    async publishToFeed(username: string, postID: string) {
        const count = await this.collection.count({ "_id": username })
        
        if (count > 0) {
            this.collection.updateOne({ "_id": username }, { $push: { "feed": { $each: [postID], $position: 0 } } })
        } else {
            this.collection.insertOne({
                "_id": username,
                "feed": [postID]
            })
        }
    }

    publishToFollowers(followersOfUsername: string, postID: string): Promise<void> {
        return new Promise(
            (resolve, reject) => {
                FollowersDB.getFollowersCursor(followersOfUsername).forEach(
                    (followers) => {
                        this.publishToFeed(followersOfUsername, postID)
                    },
                    () => {
                        resolve()
                    }
                )
            }
        )
    }



}