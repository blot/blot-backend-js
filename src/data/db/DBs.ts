import User from "./UserDB";
import Followers from "./FollowersDB";
import Following from "./FollowingDB";
import Session from "./SessionDB";
import Feeds from "./FeedsDB";
import Post from "./PostDB";
import Likes from "./LikesDB";
import LoginTokens from "./LoginTokensDB";

import ProfilePicContent from "./content/ProfilePicContentDB";

import { Db } from "mongodb";

export function initializeDBs(db: Db) {

    console.log("Initializing Databases")

    UserDB = new User(db)
    console.log("Initialized UserDB")

    FollowersDB = new Followers(db)
    console.log("Initialized FollowersDB")

    FollowingDB = new Following(db)
    console.log("Initialized FollowingDB")
    
    SessionDB = new Session(db)
    console.log("Initialized SessionDB")

    FeedsDB = new Feeds(db)
    console.log("Initialized FeedsDB")

    PostDB = new Post(db)
    console.log("Initiailized PostDB")
    
    LikesDB = new Likes(db)
    console.log("Initialized LikesDB")

    LoginTokensDB = new LoginTokens(db)
    console.log("Initialized LoginTokensDB")

    console.log("Done initializing databases")
}

export function initializeContentDBs(contentDB: Db) {

    console.log("Initializing content buckets")

    ProfilePicContentDB = new ProfilePicContent(contentDB)
    console.log("Initialized Profile Pic Bucket")

    console.log("Done initializing buckets")

}

export var UserDB: User
export var FollowersDB: Followers
export var FollowingDB: Following
export var SessionDB: Session
export var FeedsDB: Feeds
export var PostDB: Post
export var LikesDB: Likes
export var LoginTokensDB: LoginTokens

export var ProfilePicContentDB: ProfilePicContent