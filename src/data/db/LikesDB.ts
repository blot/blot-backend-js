import DataDB from "./DataDB";
import { Db } from "mongodb";

export default class LikesDB extends DataDB {

    constructor(db: Db) {
        super("likes", db)
    }

    getLikeCount(postID: string): Promise<number> {
        return new Promise(
            (resolve, reject) => {
                this.collection.aggregate([
                    { $match: { "_id": postID } },
                    { $limit: 1 },
                    { $project: { count: { $size: "$likes" } } }
                ]).limit(1).each((err, data) => {
                    if (err) reject(err)
                    resolve(data ? data.count : 0)
                })
            }
        )
    }

    likePost(username: string, postID: string): Promise<void> {
        return new Promise(
            async (resolve, reject) => {
                if (await this.isPostLikedByUser(username, postID)) reject("Post already liked by user")
                else {
                    
                    if (await this.collection.count({ "_id": postID }) > 0) {
                        this.collection.updateOne({ "_id": postID }, { $push: { "likes": username } },
                            (err, result) => {
                                if (err) reject(err)
                                else resolve()
                            }
                        )
                    } else {
                        this.collection.insertOne({
                            "_id": postID,
                            "likes": [ username ]
                        })
                    }

                }
            }
        )
    }

    unlikePost(username: string, postID: string): Promise<void> {
        return new Promise(
            async (resolve, reject) => {

                if (!(await this.isPostLikedByUser(username, postID))) reject("Post not liked by user")
                else {
                    this.collection.updateOne({ "_id": postID }, { $pull: { "likes": username } })
                        .then((res) => resolve())
                        .catch((err) => reject(err))
                }

            }
        )
    }

    isPostLikedByUser(username: string, postID: string): Promise<boolean> {
        return new Promise(
            (resolve, reject) => {
                this.collection.count({ "_id": postID, "likes": username }).then(
                    (count) => resolve(count ? count > 0 : false)
                ).catch((err) => reject(err))
            }
        )
    }

}