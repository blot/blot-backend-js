import DataDB from "./DataDB";
import { Db, Cursor } from "mongodb";

export default class FollowersDB extends DataDB {

    constructor(db: Db) {
        super("followers", db)
    }

    getFollowerCount(username: string): Promise<number> {

        return new Promise(
            (resolve, reject) => {
                this.collection.aggregate([
                    { $match: { "_id": username } },
                    { $limit: 1 },
                    { $project: { count: { $size: "$followers" } } }
                ], (err, result) => {
                    if (err) reject(err)
                    else {
                        result.each((err, data) => {
                            if (err) reject(err)
                            else resolve(data ? data.count : 0)
                        })
                    }
                })
            }
        )

    }

    getFollowedBy(username: string, toCheckUsername: string): Promise<boolean> {
        return new Promise(
            (resolve, reject) => {

                if (toCheckUsername === username) resolve(true)
                else {
                    this.collection.count({ "_id": username, "followers": toCheckUsername }, (err, count) => {
                        if (err) reject(err)

                        if (count > 0) resolve(true)
                        else resolve(false)
                    })
                }
            }
        )
    }

    addFollower(username: string, toAddUsername: string): Promise<number> {
        return new Promise(
            (resolve, reject) => {
                this.collection.count({ "_id": username }, (err, count) => {

                    // Already has followers entry
                    if (count > 0) {
                        this.collection.updateOne({ "_id": username }, { "$push": { "followers": toAddUsername } }).then(() => {
                            resolve(this.getFollowerCount(username))
                        })
                    } else { // Doesn't have a followers entry
                        this.collection.insertOne(
                            {
                                "_id": username,
                                "followers": [
                                    toAddUsername
                                ]
                            }, (err) => {
                                if (err) reject(err)

                                resolve(this.getFollowerCount(username))
                            })
                    }

                })
            }
        )
    }

    removeFollower(username: string, toRemoveUsername: string): Promise<number> {
        return new Promise(
            (resolve, reject) => {

                this.collection.count({ "_id": username }, (err, count) => {
                    if (err) reject(err)


                    if (count > 0) {
                        this.collection.updateOne({ "_id": username }, { "$pull": { "followers": toRemoveUsername }}, (err, res) => {
                            if (res.modifiedCount > 0) resolve(this.getFollowerCount(username))
                            else reject("No followers removed")
                        })
                    } else reject("User entry doesn't exist (followerCount might be 0)")

                })

            }
        )
    }

    getFollowersCursor(username: string): Cursor<{ followers: string[] }> {
        return this.collection.find({ "_id": username }, {
            projection: { "_id": 0, "followers": 0 }
        })
    }

}