import DataDB from "./DataDB";
import { Db } from "mongodb";
import BlotCrypto from "../../crypto/Crypto";

interface SessionDBSchema {
    _id: string,
    ownerID: string,
    issuedDate: string,
    lastRefreshedOn: string
}

export default class SessionDB extends DataDB {

    constructor(db: Db) {
        super("sessions", db)
    }

    deleteToken(token: string): Promise<void> {
        return new Promise(
            (resolve, reject) => {
                this.collection.deleteOne({ "_id": token })
                .then(() => resolve())
                .catch((err) => reject(err))
            }
        )
    }

    generateSessionToken(user: User): Promise<string> {
        return new Promise(
            (resolve, reject) => {
                this.generateUniqueToken().then((token) => {

                    const session: SessionDBSchema = {
                        _id: token,
                        ownerID: user.username,
                        issuedDate: new Date(Date.now()).toISOString(),
                        lastRefreshedOn: new Date(Date.now()).toISOString()
                    }

                    this.collection.insert({
                        _id: token,
                        ownerID: user.username,
                        issuedDate: new Date(Date.now()).toISOString(),
                        lastRefreshedOn: new Date(Date.now()).toISOString()
                    }, (err, res) => {
                        if (err) reject(err)

                        resolve(token)
                    })

                })
            }
        )
    }

    async getSession(token: string, refreshSessionToken: boolean = true): Promise<Session> {
        var data: SessionDBSchema = await this.collection.findOne({ "_id": token })
        if (refreshSessionToken) this.updateSession(data)

        return {
            sessionToken: data._id,
            issuedDate: data.issuedDate,
            ownerID: data.ownerID,
            lastRefreshedOn: data.lastRefreshedOn
        }
    }

    updateSession(data: SessionDBSchema) {
        this.collection.update({ "_id": data._id }, { 
            _id: data._id,
            issuedDate: data.issuedDate,
            ownerID: data.ownerID,
            lastRefreshedOn: new Date(Date.now()).toISOString() 
        })
    }

    async generateUniqueToken(): Promise<string> {
        var token = BlotCrypto.getSalt(15)
        var tokenMatchCount = await this.collection.count({ "_id": token })
        while (tokenMatchCount > 0) {
            console.warn("SessionDB : generateUniqueToken, generated token collision")
            token = BlotCrypto.getSalt(15)
            tokenMatchCount = await this.collection.count({ "_id": token })
        }
        return token
    }

}