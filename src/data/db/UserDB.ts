import DataDB from "./DataDB";
import { Db } from "mongodb";
import BlotCrypto from "../../crypto/Crypto";

export default class UserDB extends DataDB {

    constructor(db: Db) {
        super("users", db)
    }

    getUser(id: string): Promise<User> {
        return new Promise((resolve, reject) => {    
            this.collection.findOne({
                "_id": id
            }).then((res) => {
                if (res != null) {
                    resolve({
                        username: res._id,
                        displayName: res.displayName,
                        passwordSalt: res.passwordSalt,
                        passwordHash: res.passwordHash
                    })
                } else {
                    reject(`User Search returned null for user : ${id}`)
                }
            }).catch((reason) => reject(reason))
        })
    }

    createUser(id: string, displayName: string, password: string): Promise<User> {
        return new Promise(
            async (resolve, reject) => {

                if (await this.collection.count({ "_id": id }) > 0) reject("Username already exists")
                else {
                    const passwordSalt = await BlotCrypto.getCryptoSalt()
                    const passwordHash = await BlotCrypto.cryptoHash(password, passwordSalt)

                    this.collection.insertOne({
                        "_id": id,
                        "displayName": displayName,
                        "passwordSalt": passwordSalt,
                        "passwordHash": passwordHash
                    }).then(() => {
                        resolve(this.getUser(id))
                    }).catch((err) => reject(err))
                }                

            }
        )    
    }

}