import Mongo, { MongoClient, Db } from 'mongodb'
import { initializeDBs } from './DBs';
import { isInDebugMode } from '../../utils/debug';

const dbURL = getDBURL()
const dbName = "blot"

const contentDbURL = getContentDbURL()
const contentDbName = "blot-content"


function getDBURL(): string {
    if (isInDebugMode()) return "mongodb://localhost:27017"
    else return "mongodb://blot-openshift-1:9CjY0FVA7ll5jIryTCv5@ds139459.mlab.com:39459/blot"
}

function getContentDbURL(): string {
    if (isInDebugMode()) return "mongodb://localhost:27017"
    else return "mongodb://blot-openshift-1:9CjY0FVA7ll5jIryTCv5@ds139219.mlab.com:39219/blot-content"
}
export function connectToDatabase(): Promise<Db> {
    console.log(`Connecting to database [${dbURL}]`)

    return new Promise(
        (resolve, reject) => {
            MongoClient.connect(dbURL, (err, client) => {

                if (err != null) {
                    console.error(`Failed connecting to database [${err.message}]`)
                    
                    reject(err)
                } else {
                    const db = client.db(dbName)
                    console.log("Connected to database successfully")
                    
                    resolve(db)
                }

            })
        }
    )

    

}

export function connectToContentDatabase(): Promise<Db> {
    return new Promise(
        (resolve, reject) => {

            MongoClient.connect(contentDbURL, 
                (err, client) => {

                    if (err != null) {
                        console.log(`Failed connecting to content database [${err.message}]`)
                        
                        reject(err)
                    } else {
                        const db = client.db(contentDbName)
                        console.log("Connected to content database successfully")

                        resolve(db)
                    }

                }
            )

        }
    )
}