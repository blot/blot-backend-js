interface Session {

    sessionToken: string,
    ownerID: string,
    issuedDate: string,
    lastRefreshedOn: string

}