import { PostContentType } from "../processors/PostContentType";

export default interface Post {

    id: string,
    postedOn: string,
    postedBy: string,
    contentB64: string,
    contentType: PostContentType
 
}