interface User {

    username: string,
    displayName: string,
    passwordHash: string,
    passwordSalt: string

}