interface LoginToken {
    token: string
    issuedTo: string
    issuedAt: string
    lastRefreshedAt: string
}