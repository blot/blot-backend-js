import { Context, getContextUserSession } from "../Context";
import { UserDB, LikesDB } from "../db/DBs";
import Post from "../models/Post";

const PostResolver = {

    id: (post: Post, args, ctx: Context) => post.id,

    postedOn: (post: Post, args, ctx: Context) => post.postedOn,

    postedBy: (post: Post, args, ctx: Context) => UserDB.getUser(post.postedBy),

    contentB64: (post: Post, args, ctx: Context) => post.contentB64,

    contentType: (post: Post, args, ctx: Context) => post.contentType,

    likeCount: (post: Post, args, ctx: Context) => LikesDB.getLikeCount(post.id),

    likedByMe: (post: Post, args, ctx: Context) => {
        return new Promise<Boolean>(
            (resolve, reject) => {

                getContextUserSession(ctx).then(
                    (session) => {
                        resolve(LikesDB.isPostLikedByUser(session.ownerID, post.id))
                    }
                ).catch(() => resolve(false))

            }
        )
    }

}

export default PostResolver