import { getContextUserSession, Context } from "../Context";
import { FollowersDB, FollowingDB } from "../db/DBs";

const UserResolver = {

    username: (user: User, args, ctx: Context) => {
        return user.username
    },

    displayName: (user: User, args, ctx: Context) => {
        return user.displayName
    },

    followerCount: (user: User, args, ctx: Context) => {
        return FollowersDB.getFollowerCount(user.username)
    },

    followingCount: (user: User, args, ctx: Context) => {
        return FollowingDB.getFollowingCount(user.username)
    },

    followedByMe: (user: User, args, ctx: Context) => {
        return new Promise<boolean>(
            (resolve, reject) => {

                getContextUserSession(ctx).then(
                    (session) => {
                        resolve(FollowersDB.getFollowedBy(user.username, session.ownerID))
                    }
                ).catch(() => resolve(false))

            }
        )
    },

    followsMe: (user: User, args, ctx: Context) => {
        return new Promise<boolean>(
            (resolve, reject) => {

                getContextUserSession(ctx).then(
                    (session) => {
                        resolve(FollowingDB.getIfFollowing(user.username, session.ownerID))
                    }
                ).catch(() => resolve(false))

            }
        )
    }

}

export default UserResolver