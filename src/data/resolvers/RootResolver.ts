import UserResolver from "./UserResolver";
import PostResolver from "./PostResolver";
import QueryResolver from "./QueryResolver";
import MutationResolver from "./MutationResolver";

const RootResolver = {

    // Types
    User: UserResolver,
    Post: PostResolver,


    Query: QueryResolver,
    Mutation: MutationResolver,
    
}

export default RootResolver