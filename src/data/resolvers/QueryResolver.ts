import UserResolver from "./UserResolver";
import PostResolver from "./PostResolver";
import { Context, getContextUser } from "../Context";
import { UserDB, PostDB, FeedsDB } from "../db/DBs";
import Post from "../models/Post";

const QueryResolver = {

    hello: () => {
        return "Hello World"
    },

    user: (root, args: { id: string }, ctx: Context) => {
        return UserDB.getUser(args.id)
    },

    post: (root, args: { id: string }, ctx: Context) => {
        return PostDB.getPost(args.id)
    },

    me: (root, args, ctx: Context) => {
        return new Promise<User>(
            (resolve, reject) => {

                getContextUser(ctx).then(
                    (user) => resolve(user)
                ).catch((err) => reject(err))

            }
        )
    },

    getBlotsByUser: (root, args: { id: string, offset: number, limit: number }, ctx: Context) => {
        return PostDB.getPostsByUser(args.id, args.offset, args.limit)
    },

    getUserFeed: (root, args: { offset: number, limit: number }, ctx: Context) => {
        return new Promise<Post[]>(
            (resolve, reject) => {
                getContextUser(ctx).then(
                    (user) => resolve(FeedsDB.getFeedPosts(user.username, args.offset, args.limit))
                ).catch((err) => reject(err))
            }
        )
    },
}

export default QueryResolver