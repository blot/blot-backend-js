import { Context, getContextUser } from "../Context";
import { PostDB, FeedsDB, FollowingDB, FollowersDB, UserDB, LikesDB } from "../db/DBs";
import { resolvePostContentType } from "../processors/PostContentType";
import Post from "../models/Post";

const MutationResolver = {

    createPost: (root, args: { contentType: string, contentB64: string }, ctx: Context) => {
        return new Promise(
            async (resolve, reject) => {
                const user = await getContextUser(ctx)

                if (user) {
                    const post = await PostDB.createPost(resolvePostContentType(args.contentType), args.contentB64, user.username)

                    FeedsDB.publishToFollowers(user.username, post.id)

                    resolve(post)
                } else reject("Request not authenticated")
            }
        )
    },

    followUser: (root, args: { id: string }, ctx: Context) => {
        return new Promise(
            (resolve, reject) => {
                getContextUser(ctx).then(
                    (user) => {
                        if (user) {
                            if (args.id == user.username) reject("Requester is same as the request")
                            else {
                                Promise.all([
                                    FollowingDB.addFollowing(user.username, args.id),
                                    FollowersDB.addFollower(args.id, user.username)
                                ])
                                    .then(() => resolve(UserDB.getUser(args.id)))
                                    .catch((err) => reject(err))
                            }
                        } else reject("Request not authenticated")
                    }
                )
            }
        )
    },

    unfollowUser: (root, args: { id: string }, ctx: Context) => {
        return new Promise<User>(
            (resolve, reject) => {
                getContextUser(ctx).then(
                    (user) => {
                        if (user) {
                            if (args.id == user.username) reject("Requester is same as the request")
                            else {
                                Promise.all([
                                    FollowingDB.removeFollowing(user.username, args.id),
                                    FollowersDB.removeFollower(args.id, user.username)
                                ])
                                    .then(() => resolve(UserDB.getUser(args.id)))
                                    .catch((err) => reject(err))
                            }
                        } else reject("Request not authenticated")
                    }
                )
            }
        )
    },

    likePost: (root, args: { id: string }, ctx: Context) => {
        return new Promise<Post>(
            (resolve, reject) => {
                getContextUser(ctx).then(
                    (user) => {
                        if (user) {

                            LikesDB.likePost(user.username, args.id)
                                .then(() => resolve(PostDB.getPost(args.id)))
                                .catch((err) => reject(err))

                        } else reject("Request not authenticated")
                    }
                )
            }
        )
    },

    unlikePost: (root, args: { id: string }, ctx: Context) => {
        return new Promise<Post>(
            (resolve, reject) => {

                getContextUser(ctx).then(
                    (user) => {
                        if (user) {

                            LikesDB.unlikePost(user.username, args.id)
                                .then(() => resolve(PostDB.getPost(args.id)))
                                .catch((err) => reject(err))

                        } else reject("Request not authenticated")
                    }
                )
            }
        )
    }

}

export default MutationResolver