import { Application } from "express";
import { ProfilePicContentDB } from "../data/db/DBs";

export function declareContentRoute(app: Application) {

    app.get("/content/prof_pic/mini/:username", 
        (req, res) => {
            if (req.params.username) {
                ProfilePicContentDB.getProfilePictureStream(req.params.username, "mini",
                    (err) => {
                        console.error(`Mini Profile Pic fetch failed for username ${req.params.username} [${err}]`)
                        res.sendStatus(404)
                    }
                ).pipe(res)

            } else res.sendStatus(400)
        }
    )
    
    app.get("/content/prof_pic/preview/:username",
        (req, res) => {
            if (req.params.username) {

                ProfilePicContentDB.getProfilePictureStream(req.params.username, "preview", 
                    (err) => {
                        console.error(`Preview Profile Pic fetch failed for username ${req.params.username} [${err}]`)

                        res.sendStatus(404)
                    }
                ).pipe(res)

            } else res.sendStatus(400)
        }
    )

    app.get("/content/prof_pic/original/:username",
        (req, res) => {
            if (req.params.username) {

                ProfilePicContentDB.getProfilePictureStream(req.params.username, "original",
                    (err) => {
                        console.error(`Original Profile Pic fetch failed for username ${req.params.username} [${err}]`)

                        res.sendStatus(404)
                    }
                ).pipe(res)

            } else res.sendStatus(400)
        }
    )

}