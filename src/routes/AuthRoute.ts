import { Application } from "express";
import { raw } from "body-parser";
import { UserDB, ProfilePicContentDB, LoginTokensDB, SessionDB } from "../data/db/DBs";
import { request } from "http";
import BlotCrypto from "../crypto/Crypto";

export function declareAuthRoute(app: Application) {
    
    app.post("/auth/createUser", 
        async (req, res) => {
            try {
                const username = req.body.username
                const displayName = req.body.displayName
                const password = req.body.password
                const profilePictureB64 = req.body.profilePictureB64

                const user = await UserDB.createUser(username, displayName, password)
                ProfilePicContentDB.setProfilePicture(user.username, profilePictureB64)
                    .then(() => {
                        res.sendStatus(200)
                    })
                    .catch(() => {
                        res.sendStatus(200)
                    })

            } catch (e) {
                res.sendStatus(400)
            }
        }
    )

    app.get("/auth/loginWithToken",
        (req, res) => {
            
            try {

                var loginTokenString = req.header("Authorization")
                if (loginTokenString) {

                    LoginTokensDB.getToken(loginTokenString)
                        .then((loginToken) => {

                            UserDB.getUser(loginToken.issuedTo)
                                .then((user) => {

                                    SessionDB.generateSessionToken(user)
                                        .then((token) => res.send(token))
                                        .catch((err) => res.sendStatus(400))

                                })
                                .catch((err) => res.sendStatus(404))

                        })
                        .catch((err) => res.sendStatus(401))

                } else res.sendStatus(400)
            
            } catch (e) {
                res.sendStatus(400)
            }

        }
    )

    app.get("/auth/loginWithAuth",
        (req, res) => {

            try {

                const authHeader = req.header("Authorization")

                if (authHeader) {

                    if (authHeader.startsWith("Basic ")) {

                        try {
                            const usernamePasswordBase64 = authHeader.split("Basic ")[1]
                            const usernamePasswordCombo = Buffer.from(usernamePasswordBase64, 'base64').toString('utf-8').split(":")
                            const username = usernamePasswordCombo[0]
                            const password = usernamePasswordCombo[1]

                            UserDB.getUser(username)
                                .then(async (user) => {
                                    const hashedPassword = await BlotCrypto.cryptoHash(password, user.passwordSalt)

                                    if (hashedPassword == user.passwordHash) {

                                        SessionDB.generateSessionToken(user)
                                            .then((token) => {
                                                res.send(token)
                                            })

                                    } else res.sendStatus(401)
                                })
                                .catch((err) => res.sendStatus(404))
                        } catch (e) {
                            res.sendStatus(400)
                        }

                    } else res.sendStatus(400)

                } else res.sendStatus(400)

            } catch (e) {
                res.sendStatus(400)
            }

        }
    )

    app.get("/auth/getUser",
        (req, res) => {
            const sessionToken = req.header("Authorization")
            
            if (sessionToken) {

                SessionDB.getSession(sessionToken)
                    .then((session) => {
                        res.send(session.ownerID)
                    })
                    .catch((err) => res.sendStatus(404))

            } else res.sendStatus(401)
        }
    )

    app.get("/auth/createLoginToken",
        (req, res) => {
            const sessionToken = req.header("Authorization")

            if (sessionToken) {
                SessionDB.getSession(sessionToken)
                    .then((session) => {
                        LoginTokensDB.createToken(session.ownerID)
                            .then((loginToken) => res.send(loginToken.token))
                            .catch((err) => res.sendStatus(401))
                    })
                    .catch((err) => res.sendStatus(404))
            } else res.sendStatus(400)
        }
    )

    app.get("/auth/closeSession",
        (req, res) => {
            const sessionToken = req.header("Authorization")

            if (sessionToken) {
                SessionDB.deleteToken(sessionToken)
                    .then(() => res.sendStatus(200))
                    .catch(() => res.sendStatus(404))
            } else res.sendStatus(400)
        }
    )

}