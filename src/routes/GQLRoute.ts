import { Express } from 'express-serve-static-core'
import { buildSchema, graphql } from 'graphql'
import graphqlHTTP from 'express-graphql'
import RootResolver from '../data/resolvers/RootResolver'
import { setRequireExtensions } from '../utils/RequireExtensions';
import { makeContext } from '../data/Context';
import { NextFunction, Application } from 'express';
import { makeExecutableSchema } from 'graphql-tools'
import { isInDebugMode } from '../utils/debug';

setRequireExtensions()

var schemaText = require("../../res/schemas/schema.graphql")
//var schema = buildSchema(schemaText)
var schema = makeExecutableSchema({
    typeDefs: schemaText,
    resolvers: RootResolver
})

function declareGQLRoute(app: Application) {
    app.use('/gql', graphqlHTTP(async request => {
        return {
            schema: schema,
            context: makeContext(request),
            graphiql: isInDebugMode()
        }
    }));
}

export default declareGQLRoute